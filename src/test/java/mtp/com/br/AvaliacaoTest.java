package mtp.com.br;
import org.junit.*;
import mtp.com.br.Avaliacao;
import mtp.com.br.IAluno;
import static org.mockito.Mockito.*;

public class AvaliacaoTest {

	Avaliacao aval = new Avaliacao();
	
	@Test
	public void testAlunoAprovado() {
		IAluno aluno = mock(IAluno.class);
		when(aluno.getNotaP1()).thenReturn(1.0);
		when(aluno.getNotaP2()).thenReturn(1.0);
		when(aluno.getNotaTrabalho()).thenReturn(4.0);
		
		
		String resultado = aval.avaliar(aluno);
		// Teste Aprovado
		Assert.assertEquals("Aprovado", resultado);
		
	}
}
